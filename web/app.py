from flask import Flask, render_template, abort, request
import os

app = Flask(__name__)

@app.route("/")
def hello():
	return "UOCIS docker demo!"

@app.route("/<response>")
def respond(response):
	path = "./templates/" + response
	if ".." in response or "//" in response or "~" in response:
		abort(403)
	elif os.path.isfile(path) == False:
		abort(404)
	else:
		return render_template("./" + response)

@app.errorhandler(404)
def error_404(err):
	urlPth = request.full_path

	if ".." in urlPth or "//" in urlPth or "~" in urlPth:
		return render_template("403.html"), 403
	return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(err):
	return render_template("403.html"), 403

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
